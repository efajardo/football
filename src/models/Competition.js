var mongoose = require('mongoose')
let Schema = mongoose.Schema

let competitonSchema = new Schema({
  id: Number, 
  name: String,
  country: String,
  teams: [{
    type: Schema.Types.ObjectId,
    ref: 'team'
  }],

}, { versionKey: false })

let Competition = mongoose.model('competition', competitonSchema)

module.exports = Competition