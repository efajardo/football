var mongoose = require('mongoose')
let Schema = mongoose.Schema

let teamSchema = new Schema({
  name: String,
  id: Number,
  address: String,
  phone: String,
  website: String,
  clubColors: String,
  competitions: [{
    type: Schema.Types.ObjectId,
    ref: 'competition'
  }],
  players: [{
    type: Schema.Types.ObjectId,
    ref: 'player'
  }]
}, { versionKey: false })

let Team = mongoose.model('team', teamSchema)

module.exports = Team