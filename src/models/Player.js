var mongoose = require('mongoose')
let Schema = mongoose.Schema

let playerSchema = new Schema({
  id: Number,
  name: String,
  position: String,
  role: String
}, { versionKey: false })

let Player = mongoose.model('player', playerSchema)

module.exports = Player