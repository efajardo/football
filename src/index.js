const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const apiRoutes = require('./routes')
const cron = require('node-cron')

app.use(bodyParser.urlencoded({ extended: false }));

const footballdata = require('./controllers/footballdata')

const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/football', { useNewUrlParser: true })

let db = mongoose.connection
db.on('error', console.error.bind(console, 'Connection error:'))
db.once('open', () => {
  console.log("BD Conectada")
})

//Ejecutamos request inicial y se define cron cada 10 minutos
footballdata.getCompetitions()
cron.schedule('*/10 * * * *', () => {
  footballdata.getCompetitions()
})

//Definimos ruta para endpoints
app.use('/api', apiRoutes)

app.listen(3000, () => {
  console.log('Servidor corriendo en puerto 3000.')
})
