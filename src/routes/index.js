var express = require('express');
var isOnline = require('../lib/is-online')
var api = express.Router();

const competitionsCtrl = require('../controllers/competitions')
const teamsCtrl = require('../controllers/teams')
const playersCtrl = require('../controllers/players')

//ENDPOINTS 

//Competitions
api.get('/competitions', competitionsCtrl.getAll)
api.get('/competitions/:id', competitionsCtrl.getCompetition)

//Team
api.get('/team', teamsCtrl.getAll)
api.get('/team/:id', teamsCtrl.getTeam)
api.post('/team', isOnline, teamsCtrl.save)

//Player
api.get('/players', playersCtrl.getAll)


module.exports = api