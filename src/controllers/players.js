const Player = require('../models/Player')

async function getAll(req, res){
  //Retornar todos los jugadores
  try{
    let players = await Player.find()
    if(!players){
      return res.status(200).send({message: 'No existen jugadores cargados'})  
    }

    res.status(200).send({players})
  } catch(error){
    res.status(500).send({message: error.message})
  }
}

module.exports = {
  getAll
} 