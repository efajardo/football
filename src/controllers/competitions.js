const Competition = require('../models/Competition')

async function getAll(req, res){  
  //Retorno todas competiciones guardadas junto a sus asociaciones (Equipos>Jugadores)
  try {
    let competitions = await Competition.find()
                                        .populate({
                                          path: 'teams',
                                          select: {competitions: 0},
                                          populate: ({
                                            path: 'players'
                                          })
                                        })

    if(!competitions){
      return res.status(200).send({message: 'No existen competiciones cargadas'})  
    }
    res.status(200).send({competitions})
  } catch(error) {
    res.status(500).send({message: error.message})
  }
}

async function getCompetition(req, res){
  //Retorno competicion segun ID junto a sus asociaciones (Equipos>Jugadores)
  try {
    let competitionId = req.params.id
    let competition = await Competition.findOne({id: competitionId})
                                        .populate({ 
                                          path: 'teams',
                                          select: {competitions: 0},
                                          populate: {
                                            path: 'players'
                                          } 
                                        })

    if(!competition){
      return res.status(404).send({message: 'Liga no encontrada'})
    }
    res.status(200).send({competition})

  } catch(error) {
    res.status(500).send({message: error.message})
  }
}

module.exports = {
  getAll,
  getCompetition
}