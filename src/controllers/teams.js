const Team = require('../models/Team')
const Competition = require('../models/Competition')
const Player = require('../models/Player')
const request = require('../lib/request')
var ObjectId = require('mongoose').Types.ObjectId;

async function getAll(req, res){
  //Obtener listado general de equipos.
  try {
    let teams = await Team.find()
                          .populate([
                            {
                              path: 'competitions',
                              select: {teams: 0}
                            },
                            { path: 'players' }
                          ])
    
    if(!teams.length){
      return res.status(200).send({message: 'No existen equipos cargados'})  
    }
    res.status(200).send({teams})
  } catch (error) {
    res.status(500).send({message: error.message})
  }
}

async function getTeam(req, res){
  //Obtener equipo según ID
  try {
    let teamId = req.params.id
    let team = await Team.findOne({id: teamId})
                          .populate([
                            { 
                              path: 'competitions',
                              select: {teams: 0}
                            }, 
                            { path: 'players' }
                          ])
    
    if(!team){
      return res.status(404).send({message: 'Equipo no encontrado'})
    }

    res.status(200).send({team})  
  } catch (error) {
    res.status(500).send({message: error.message})
  }
}

function save(req, response){

  if(!req.body.id) return res.status(404).send({message: 'Debes indicar el ID del equipo'})
  let teamId = req.body.id

  //Ejecutamos request para obtener equipo según ID y con ello se realiza 
  //las asociaciones (Competicion>Equipo Equipo>Jugadores,Competiciones)
  request({method: 'get', url: `teams/${teamId}`})
  .then(async res => {
    try {
      
      let team = await Team.findOne({id: teamId})

      let dataTeam = {
        id: teamId,
        name: res.name,
        address: res.address,
        phone: res.phone,
        website: res.website,
        clubColors: res.clubColors,
        players: [],
        competitions: []
      }

      if(!team){

        let team = new Team(dataTeam)
    
        let promise = res.activeCompetitions.map(async activeComp => {
          try{
            let competition = await Competition.findOne({id: activeComp.id})
            competition.teams.push(team)
            competition.save()

            return competition
          } catch(error){
            response.status(500).send({message: error.message})
          }
        })
        const competitions = await Promise.all(promise)
        //Almaceno competiciones activas del equipo
        team.competitions = competitions

        res.squad.map( squad => {
          let player = new Player({
            id: squad.id,
            name: squad.name, 
            position: squad.position,
            role: squad.role
          })
          //Almaceno jugador
          player.save()
          //Almaceno jugadores del equipo
          team.players.push(player)
        })

        team.save(error => {
          if(error) return res.status(500).send({message: error.message})
          response.status(200).send({message: 'Equipo guardado exitosamente'})
        })

      } else {
        //Cargamos nuevamente los jugadores y competencias, 
        //esto debido a que pueden existir cambios en el elenco 
        //o en las competencias (eliminados o clasificacion)
        team.competitions.map(competition => {
          Competition.update({_id: competition._id}, { $pull : { teams: new ObjectId(team._id) } }, {safe: true}, (err, competitionUpdated) => {
          })
        })

        let promisePlayer = res.squad.map(async squad => {
          try {
            let player = await Player.findOne({id: squad.id})
            return player
          } catch (error) {
            response.status(500).send({message: error.message})
          }
        })
        //Incorporo nuevamente a sus últimos jugadores
        const players = await Promise.all(promisePlayer)
        dataTeam.players = players

        let promiseCompetition = res.activeCompetitions.map(async activeComp => {
          try {
            let competition = await Competition.findOne({id: activeComp.id})
            //Incorporo al equipo en sus competencias actuales
            competition.teams.push(team)
            competition.save()

            return competition
          } catch (error) {
            response.status(500).send({message: error.message})
          }
        })
        const competitions = await Promise.all(promiseCompetition)
        dataTeam.competitions = competitions

        Team.updateOne({id: teamId}, { $set: dataTeam }, (err, teamUpdated) => {
          if(err) return response.status(500).send({message: err.message})
          response.status(200).send({message: 'Equipo actualizado exitosamente'})
        })
        
      }
    } catch(error) {
      console.error(error)
      response.status(500).send({message: error.message})
    }
    
  })
  .catch(error => {
    //Retorno error (mensaje y errorCode) de la API Football
    if(error.data) response.status(error.data.errorCode).send({message: error.data.message})
  })
}

module.exports = {
  getAll,
  getTeam,
  save
} 