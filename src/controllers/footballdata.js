const isOnline = require('is-online')
const request = require('../lib/request')
const Competition = require('../models/Competition')

function getCompetitions(){

  isOnline().then(online => {
    if(online){
      request({method: 'get', url: `competitions/`}).then(async res => {

        let promises = res.competitions.map( async competition => {
    
          let data = {
            id: competition.id,
            name: competition.name,
            country: competition.area.name
          }
    
          let comp = await Competition.findOne({id: competition.id})
          if(comp){
            data._id = comp._id
          } 
          return data
        })
        
        const results = await Promise.all(promises)
        
        //Filtro la data para revisar si existe o no.
        //Si existe se actualiza, caso contrario se inserta
        let update_data = results.filter( item => { return '_id' in item })
        let new_data = results.filter(item => { return !('_id' in item) })
        
        //console.log("total INS: ", new_data.length)
        if(new_data.length){
          try{
            Competition.collection.insertMany(new_data, (err, res) => {
              if(err){
                console.log(`Error: ${err}`)
              }
              console.log(`Se han guardado ${res.insertedCount} Competiciones.`)
            })
          } catch(e){
            console.log(`Error: ${e}`)
          }
        }
    
        //console.log("total UDP: ", update_data.length)
        if(update_data.length){
          try{
            Competition.updateMany({}, { update_data }, (err, res) => {
              
              if(err){
                console.log(`Error: ${err}`)
              }
              console.log(`Se han actualizado ${update_data.length} Competiciones.`)
            })
          } catch(e){
            console.log(`Error: ${e}`)
          }
        }
        
      }).catch(error => {
        if(error.data){
          //Retorno error (mensaje y errorCode) de la API Football
          console.error(`Error: ${error.data.message}, Status ${error.data.errorCode}`)
        } else {
          console.error(`Error: ${error}`)
        }
      })
    } else {
      console.error('Revise conexión a internet')
    }
  })
}

module.exports = {
  getCompetitions
}