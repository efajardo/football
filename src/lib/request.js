const axios = require('axios')
const config = require('../config')

//Utilizo los datos guardados en config para encapsular los request
const client = axios.create({
  baseURL: config.apiUrl,
  headers: {
    'X-Auth-Token': config.apiToken
  }
});

const request = function(options) {
  const onSuccess = function(response) {
    return response.data;
  }

  const onError = function(error) {
    return Promise.reject(error.response || error.message);
  }

  return client(options)
          .then(onSuccess)
          .catch(onError);
}

module.exports = request

