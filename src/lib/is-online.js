const isOnline = require('is-online')

//Funcion que utilizo como middleware de las rutas que hacen request a la api de football-data
function online(req, res, next){
  isOnline().then(online => {
    if(!online){
      console.error('Revise conexión a internet')
      res.status(500).send({message: 'Revise conexión a internet'})
    }
    next()
  })
}

module.exports = online